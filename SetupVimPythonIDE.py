#!/usr/bin/python3

import os


pathToFontsConfig = ""
if os.path.exists("~/.fonts.conf.d/"):
    pathToFontsConfig = "~/.fonts.conf.d/"
else:
    pathToFontsConfig = "~/.config/fontconfig/conf.d/"

commands = [
    "git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle",
    "git clone https://github.com/Lokaltog/powerline.git ~/.vim/bundle/powerline",
    "git clone https://github.com/Lokaltog/powerline-fonts.git  ~/.fonts/",
    "wget -P ~/.fonts/ https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf",
    "wget -P " + pathToFontsConfig + " https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf",
    "fc-cache -vf ~/.fonts",
    "git clone https://github.com/tpope/vim-fugitive.git ~/.vim/bundle/vim-fugitive",
    "git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree",
    "git clone git://github.com/klen/python-mode.git ~/.vim/bundle/python-mode"
]

def run_command(command):
    stream = os.popen(command)
    return stream.read().strip()

print("Clonning files from git...")
for command in commands:
    run_command(command)

print("End clonning...")

print("Reading vim configuraton")
currentDir = os.path.dirname(__file__)
settingsFile = open(currentDir + "/settings.txt", "r", encoding="utf-8")
settings = "\n" + "".join(settingsFile.readlines())
settingsFile.close()
print("Vim configuration reading is end")

print("Setting vim configuration...")
vimrcFile = open(os.path.expanduser(".vimrc"), "a", encoding="utf-8")
vimrcFile.write(settings)
vimrcFile.close()
print("Setting vim configuration is end")
